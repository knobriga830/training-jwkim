
var tui = require('tui-code-snippet');
var util = require('./util');

var ToolTipItem = tui.defineClass({
    init: function(options) {
        this.selectorName = options.selectorName;
        this.contents = options.contents;
        this.delay = options.delay;
        this.targetElements = options.targetElements;
        this.onMouseOverHandler = options.onMouseOverHandler;
        this.onMouseLeaveHandler = options.onMouseLeaveHandler;
        this.timeout = null;
    },
    addToolTipEvent: function(targetElement) {
        util.addEventListener(targetElement || this.targetElements, 'mouseenter', this.onMouseOverHandler);
        util.addEventListener(targetElement || this.targetElements, 'mouseleave', this.onMouseLeaveHandler);
    },
    removeTooltipEvent: function(targetElement) {
        util.removeEventListener(targetElement || this.targetElements, 'mouseenter', this.onMouseOverHandler);
        util.removeEventListener(targetElement || this.targetElements, 'mouseleave', this.onMouseLeaveHandler);
    },
    addToolTipElement: function(targetElement) {
        var tooltipElement = document.createElement('div');

        if (this.wrapingForSelfClosingTag(targetElement)) {
            return false;
        }

        tooltipElement.className = 'tooltip';
        tooltipElement.appendChild(document.createTextNode(this.contents));
        targetElement.appendChild(tooltipElement);

        return true;
    },
    removeToolTipElement: function(targetElement) {
        targetElement.removeChild(targetElement.querySelector('.tooltip'));
    },
    /**
     * wrapping tag for tooltip
     * @param {HTMLElement} targetElement - tooltip html element
     * @returns {HTMLElement}  - remake tooltip html element
     */
    wrapingForSelfClosingTag: function(targetElement) {
        var newTargetElement = null;

        if (targetElement.tagName.toLowerCase() === 'img') {
            this.removeTooltipEvent(targetElement);
            newTargetElement = document.createElement('span');
            newTargetElement.style = 'position:relative';
            newTargetElement.appendChild(targetElement.cloneNode());
            targetElement.parentNode.insertBefore(newTargetElement, targetElement);
            targetElement.parentNode.removeChild(targetElement);
            targetElement = newTargetElement;
            this.addToolTipEvent(targetElement);
        }

        return newTargetElement;
    }

});

module.exports = ToolTipItem;
