
var util = {

    /**
     * add event listener
     * @param {HTMLNodeList} nodeList - HTMLNodelist
     * @param {string} eventName - tooltip event selector name
     * @param {mouseEventHandler} eventHandler - add handler
     */
    addEventListener: function(nodeList, eventName, eventHandler) {
        [].forEach.call(nodeList, function(node) {
            node.addEventListener(eventName, eventHandler);
            if (eventName === 'mouseenter') {
                this.addClass(node, 'tooltip-event');
            }
        }.bind(this));
    },
    /**
     * add event listener
     * @param {HTMLNodeList} nodeList - HTMLNodelist
     * @param {string} eventName - tooltip event selector name
     * @param {mouseEventHandler} eventHandler - add handler
     */
    removeEventListener: function(nodeList, eventName, eventHandler) {
        [].forEach.call(nodeList, function(node) {
            node.removeEventListener(eventName, eventHandler);
            if (eventName === 'mouseenter') {
                this.removeClass(node, 'tooltip-event');
            }
        }.bind(this));
    },

    /**
     * find element target
     * @param {EventObject} event - eventObject
     * @returns {HTMLElement} eventTarget = target element
     * @param {boolean} imgChkPass - image
     */
    findEventTarget: function(event) {
        var eventTarget = null;
        event = window.event || event;
        eventTarget = event.target;

        if (eventTarget.tagName.toLowerCase() === 'img' && !eventTarget.className.match(/tooltip-event/)) {
            eventTarget = eventTarget.parentNode;
        }

        return eventTarget;
    },
    /**
     * add Class
     * @param {HTMLElement} element = target element
     * @param {string} className - class name
     */
    addClass: function(element, className) {
        element.className += ' ' + className;
    },
    /**
     * add Class
     * @param {HTMLElement} element = target element
     * @param {string} className - class name
     */
    removeClass: function(element, className) {
        var check = new RegExp('(\\s|^)' + className + '(\\s|$)');
        element.className = element.className.replace(check, ' ').trim();
    }

};

module.exports = util;
