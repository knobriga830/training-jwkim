module.exports = {
    "extends": "tui",
    "parserOptions": {
        "ecmaVersion": 3,
        "sourceType": "module"
    },
    "env": {
        "commonjs": true,
        "jasmine": true,
        "amd": true,
        "node": true,
        "jquery": true,
        "es6": true

    },
    "globals": {
        "loadFixtures": true
    },
    "rules": {
        "dot-notation": 0
    }
}
