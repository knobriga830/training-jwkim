
export default class TodoItem {
    /**
     * initialize
     * @param {string} todoname - todo name
     * @param {number} seq - lastSeq of list
     */
    constructor(todoname, seq) {
        this.seq = seq;
        this.todoname = todoname;
        this.createDt = new Date().getTime();
        this.complete = false;
    }
    /**
     * toggle complete state
     */
    toggleComplete() {
        this.complete = !this.complete;
    }
}
